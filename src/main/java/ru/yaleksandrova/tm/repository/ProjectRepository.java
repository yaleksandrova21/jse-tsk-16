package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Comparator;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(Comparator<Project> projectComparator) {
        final List<Project> projects = new ArrayList<>(this.projects);
        projects.sort(projectComparator);
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public int size() {
        return projects.size();
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public Project findById(final String id) {
        for(Project project:projects){
            if(id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(String name) {
        for(Project project:projects){
            if(name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}

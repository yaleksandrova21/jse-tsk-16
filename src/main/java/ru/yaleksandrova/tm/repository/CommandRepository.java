package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.ICommandRepository;
import ru.yaleksandrova.tm.constant.ApplicationConst;
import ru.yaleksandrova.tm.constant.ArgumentConst;
import ru.yaleksandrova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            ApplicationConst.ABOUT, ArgumentConst.ABOUT, "Display developer info..."
    );

    public static final Command HELP = new Command(
            ApplicationConst.HELP, ArgumentConst.HELP, "Display list of commands..."
    );

    public static final Command VERSION = new Command(
            ApplicationConst.VERSION, ArgumentConst.VERSION, "Display program version..."
    );

    public static final Command INFO = new Command(
            ApplicationConst.INFO, ArgumentConst.INFO, "Display system information..."
    );

    public static final Command EXIT = new Command(
            ApplicationConst.EXIT, null, "Close application..."
    );

    public static final Command ARGUMENTS = new Command(
            ApplicationConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display list arguments..."
    );

    public static final Command COMMANDS = new Command(
            ApplicationConst.COMMANDS, ArgumentConst.COMMANDS, "Display list commands..."
    );

    public static final Command TASK_LIST = new Command(
            ApplicationConst.TASK_LIST, null, "Show task list"
    );

    public static final Command TASK_CREATE = new Command(
            ApplicationConst.TASK_CREATE, null, "Create new task"
    );

    public static final Command TASK_CLEAR = new Command(
            ApplicationConst.TASK_CLEAR, null, "Clear all tasks"
    );

    public static final Command PROJECT_LIST = new Command(
            ApplicationConst.PROJECT_LIST, null, "Show project list"
    );

    public static final Command PROJECT_CREATE = new Command(
            ApplicationConst.PROJECT_CREATE, null, "Create project task"
    );

    public static final Command PROJECT_CLEAR = new Command(
            ApplicationConst.PROJECT_CLEAR, null, "Clear all projects"
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            ApplicationConst.PROJECT_SHOW_BY_ID, null, "Show project by id"
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            ApplicationConst.PROJECT_SHOW_BY_INDEX, null, "Show project by index"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            ApplicationConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            ApplicationConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            ApplicationConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            ApplicationConst.PROJECT_UPDATE_BY_ID, null, "Update project by id"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            ApplicationConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            ApplicationConst.PROJECT_START_BY_ID, null, "Start project by id"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            ApplicationConst.PROJECT_START_BY_INDEX, null, "Start project by index"
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            ApplicationConst.PROJECT_START_BY_NAME, null, "Start project by name"
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            ApplicationConst.PROJECT_FINISH_BY_ID, null, "Finish project by id"
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            ApplicationConst.PROJECT_FINISH_BY_INDEX, null, "Finish project by index"
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            ApplicationConst.PROJECT_FINISH_BY_NAME, null, "Finish project by name"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            ApplicationConst.PROJECT_CHANGE_STATUS_BY_ID, null, "Change project status by id"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            ApplicationConst.PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change project status by index"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_NAME = new Command(
            ApplicationConst.PROJECT_CHANGE_STATUS_BY_NAME, null, "Change project status by name"
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            ApplicationConst.TASK_SHOW_BY_ID, null, "Show task by id"
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            ApplicationConst.TASK_SHOW_BY_INDEX, null, "Show task by index"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            ApplicationConst.TASK_REMOVE_BY_ID, null, "Remove task by id"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            ApplicationConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            ApplicationConst.TASK_REMOVE_BY_NAME, null, "Remove task by name"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            ApplicationConst.TASK_UPDATE_BY_ID, null, "Update task by id"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            ApplicationConst.TASK_UPDATE_BY_INDEX, null, "Update task by index"
    );

    private static final Command TASK_START_BY_ID = new Command(
            ApplicationConst.TASK_START_BY_ID, null, "Start task by id"
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            ApplicationConst.TASK_START_BY_INDEX, null, "Start task by index"
    );

    private static final Command TASK_START_BY_NAME = new Command(
            ApplicationConst.TASK_START_BY_NAME, null, "Start task by name"
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            ApplicationConst.TASK_FINISH_BY_ID, null, "Finish task status by id"
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            ApplicationConst.TASK_FINISH_BY_INDEX, null, "Finish task by index"
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            ApplicationConst.TASK_FINISH_BY_NAME, null, "Finish task by name"
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            ApplicationConst.TASK_CHANGE_STATUS_BY_ID, null, "Change task status by id"
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            ApplicationConst.TASK_CHANGE_STATUS_BY_INDEX, null, "Change task status by index"
    );

    private static final Command TASK_CHANGE_STATUS_BY_NAME = new Command(
            ApplicationConst.TASK_CHANGE_STATUS_BY_NAME, null, "Change task status by name"
    );

    private static final Command TASK_LIST_SHOW_BY_PROJECT_ID = new Command(
            ApplicationConst.TASK_LIST_SHOW_BY_PROJECT_ID, null, "Show task list by project Id"
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(
            ApplicationConst.TASK_BIND_TO_PROJECT, null, "Bind task to project"
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            ApplicationConst.TASK_UNBIND_FROM_PROJECT, null, "Unbind task from project"
    );

    public static final Command[] APPLICATION_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR, PROJECT_LIST,
            PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_NAME,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_NAME,
            TASK_LIST_SHOW_BY_PROJECT_ID, TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,
            EXIT
    };

    public Command[] getCommands() {
        return APPLICATION_COMMANDS;
    }

}

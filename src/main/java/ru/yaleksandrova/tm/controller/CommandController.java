package ru.yaleksandrova.tm.controller;

import ru.yaleksandrova.tm.api.sevice.ICommandService;
import ru.yaleksandrova.tm.api.controller.ICommandController;
import ru.yaleksandrova.tm.exception.system.UnknownCommandException;
import ru.yaleksandrova.tm.model.Command;
import ru.yaleksandrova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yulia Aleksandrova");
        System.out.println("E-mail: yaleksanrova@yandex.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command: commands) {
            showCommandValue(command.getName());
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command: commands) {
            showCommandValue(command.getArgument());
        }
    }

    @Override
    public void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }
    @Override
    public void showErrorCommand() {
        throw new UnknownCommandException();
    }

    @Override
    public void showErrorArgument() {
        System.err.println("Error! Argument not supported...");
        System.exit(1);
    }

    @Override
    public void exitApplication() {
        System.exit( 0 );
    }

}

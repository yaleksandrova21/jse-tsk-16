package ru.yaleksandrova.tm.enumerated;

import java.util.Comparator;
import ru.yaleksandrova.tm.comparator.ComparatorByCreated;
import ru.yaleksandrova.tm.comparator.ComparatorByName;
import ru.yaleksandrova.tm.comparator.ComparatorByStatus;
import ru.yaleksandrova.tm.comparator.ComparatorByStartDate;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

}
